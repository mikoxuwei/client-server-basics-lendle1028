/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lendle.courses.network.clientserverbasics;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author miko
 */
public class HttpServer {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // TODO code application logic here
        ServerSocket serverSocket = new ServerSocket(8888);
        while (true) {
            Socket client = serverSocket.accept();
            OutputStream out = client.getOutputStream();
            OutputStreamWriter write = new OutputStreamWriter(out, "utf-8");
            write.write("HTTP/1.0 200 OK\r\n\r\n");
            write.write("<h1>Hello~<h1>");
            write.write("<html><head><meta charset='utf-8'></head><p>你沒事就別一直重整，我會掛掉 ˋ^ˊ</p></html>");
            write.flush();
            client.close();
//            while (true) {} //開著瀏覽器會一直跑，會死掉，因為無法再回 accept      
//            Thread.sleep(30000); //睡30秒
            
        }
    }
    
}
