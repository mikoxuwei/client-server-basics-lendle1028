/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class DictClient1 {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
         // TODO code application logic here
            Socket socket = new Socket("dict.org", 2628);
            InputStream input = socket.getInputStream();
            OutputStream output = socket.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(output);
            
            writer.write("DEFINE wn network\r\n");
            writer.flush();
            
            InputStreamReader inputStreamReader = new InputStreamReader(input);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            for (String line = reader.readLine(); !line.equals("."); line = reader.readLine()) {
                System.out.println(line);
            }
            writer.write("quit\r\n");//Il0O
        
    }

}