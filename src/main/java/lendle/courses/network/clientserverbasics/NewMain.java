/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lendle.courses.network.clientserverbasics;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author miko
 */
public class NewMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        try ( // TODO code application logic here
            Socket s = new Socket("time.nist.gov", 13)) {
                InputStream input = s.getInputStream();
                StringBuilder builder = new StringBuilder();
                InputStreamReader reader = new InputStreamReader(input);
                for(int c = reader.read(); c != -1; c = reader.read()){
                    builder.append((char)c);
                }
                System.out.println(builder);
                s.close();
        }
    }
    
}
